<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title><?php echo   $wp_query->post->post_title;  ?></title>
    <?php wp_enqueue_script("jquery"); ?>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script src="https://cdn.polyfill.io/v2/polyfill.js?features=Element.prototype.closest"></script>
    <?php wp_head();?>
</head>
<body>
<?php get_header()?>
    <div class="heading">
        <div class="container">
            <div class="heading__body">
                <div class="heading__contant ">
                <h1 class="heading__h1 head_big_text"><?php echo   $wp_query->post->post_title;  ?></h1>
                    <div class="heading__way"> <p><?php echo get_the_title(  get_page_by_path( 'main' ) ); ?> >  <?php echo   $wp_query->post->post_title;  ?> </p></div>
            </div>
            </div>

        </div>
    </div>
<div class="categories__burger">
    <h2 class="categories__heading ">Катигории</h2>
</div>
<div class="container">
    <div class="contant">
<section class="categories">
<div class="categories__body">
        <div class="categories__collum">



<?php 
$list_category_slag = ['bakery-products','hoter','breakfasts','porridge','pasta','salads','soups','sandwich-rolls','sandwichs','cakes-corners','scrambled_eggs'];
$list_category_ID = array();
foreach($list_category_slag as $slag){
    $cat = get_category_by_slug($slag); 
    $id = $cat->term_id;
    array_push($list_category_ID,$id);
};
unset($cat);
unset($id);
$categories = get_categories( [
	'taxonomy'     => 'category',
	'type'         => 'post',
	'child_of'     => 0,
	'parent'       => '',
	'orderby'      => 'name',
	'order'        => 'ASC',
	'hide_empty'   => 0,
	'hierarchical' => 1,
	'exclude'      => '1',
	'include'      => implode(",",$list_category_ID),
	'number'       => 0,
	'pad_counts'   => false,
] );


if( $categories ):
	foreach( $categories as $cat ):

		// Данные в объекте $cat

		// $cat->term_id
		// $cat->name (Рубрика 1)
		// $cat->slug (rubrika-1)
		// $cat->term_group (0)
		// $cat->term_taxonomy_id (4)
		// $cat->taxonomy (category)
		// $cat->description (Текст описания)
		// $cat->parent (0)
		// $cat->count (14)
		// $cat->object_id (2743)
		// $cat->cat_ID (4)
		// $cat->category_count (14)
		// $cat->category_description (Текст описания)
		// $cat->cat_name (Рубрика 1)
		// $cat->category_nicename (rubrika-1)
		// $cat->category_parent (0)

 ?>

                <?php if($cat->slug=='breakfasts'):?>
                    <div class="categories__item categories__item_active" id ="<?php echo $cat->cat_ID; ?>">
                        <h2 class="categories__heading <?php echo $cat->cat_ID; ?>"><?php echo $cat->name; ?></h2>
                        <p class="categories__addInform "><?php echo $cat->category_description ?></p>
                    </div>
                <?php else:?>
                <div class="categories__item " id ="<?php echo $cat->cat_ID; ?>">
                        <h2 class="categories__heading" "><?php echo $cat->name; ?></h2>
                        <p class="categories__addInform "><?php echo $cat->category_description ?></p>
                    </div>
                <?php endif;?>
                
<?php endforeach; ?>  
<?php endif?>
</div>
            </div>
    </section>
<main  class="products">
     <div class="products__navigation"><p class="card__subtext"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/vector_smart_object.png"> Назад в меню</p></div>
    <div class="products__body">
        <div class="card__top">
        </div>
        <div class="card__titleRecommendation">Рекоментую</div>
        <?php
//сколько выводим постов 
$args=array(
    'post_type' => 'products',
    'showposts'=> 3,
    'category_name'=> 'breakfasts'
);
//запрос постов 
$whole_cake = query_posts($args);
global $whole_cake;
foreach ($whole_cake as $post) :

setup_postdata($post);

$thumbnail_attributes = wp_get_attachment_image_src( get_post_thumbnail_id(), 'medium' );

$custom_field_keys = get_post_custom_keys();

$page_id = get_the_ID();

$card__message ='';
$card__Weight = '';
$card__Price = '';

foreach ( $custom_field_keys as $key => $value ) {
	$valuet = trim($value);
		if ( '_' == $valuet{0} )
			continue;
            
    switch($valuet){
        case "card__message":
            $card__message = get_post_meta($page_id, $value, true);
            break;
        case "card__Weight":
            $card__Weight = get_post_meta($page_id, $value, true);
            break;
        case "card__Price":
            $card__Price = get_post_meta($page_id, $value, true);
            break;
         }
	//echo $key .' => '. $value . '<br />';
}
?>

<div class="card">
    <div class="card__body">
        <div class="card__header">
            <picture><source srcset="<?php echo $thumbnail_attributes[0]; ?>" type="image/webp"><img src="<?php echo $thumbnail_attributes[0]; ?>" alt="" class="card__img"></picture>
        </div>

    <div class="card__main">
        <div class="card__heading">
            <h3>  <?php the_title(); ?></a></h3> <h2 class="card__message">
            <?php echo $card__message ;?></h2>
        </div>
        <div class="card__text">
            <p class="card__subtext">  <?php echo get_the_content(); ?></a>  </p>
        </div>
            <div class="card__fooder">
            <p class="card__WeightPrice"> <?php echo  $card__Weight ;?></p>
            <p class="card__WeightPrice"><?php echo $card__Price ;?> <span>у</span></p>
            </div>
        </div>
    </div>
</div>
     
<?php endforeach; 
?> 

    </div>
<?php
    global $wp_query;
 
// текущая страница
$paged = get_query_var( 'paged' ) ? get_query_var( 'paged' ) : 1;
// максимум страниц
$max_pages = $wp_query->max_num_pages;
 
// если текущая страница меньше, чем максимум страниц, то выводим кнопку
if( $paged < $max_pages ) {
    echo ('<div id="loadmore" style="text-align:center;">
    <a hidden  href="#" data-max_pages="' . $max_pages . '" data-paged="' . $paged . '" data-post_type = "products" data-categories ="5"  class="button">Загрузить ещё</a>
</div>');
}
?>
   
</main>
</div>
</div>

<?php get_footer();?>
<?php wp_footer();?>
</body>

</html>