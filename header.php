<header class="header">
    <div class="menu ">

        <div class="menu__body">
            <div class="menu__burger">
                <img src="https://img2.freepng.ru/20180429/okq/kisspng-computer-icons-menu-cafe-5ae5cda75d2bc2.8582560415250098313816.jpg" >
            </div>
            <?php wp_nav_menu( array(
                'theme_location' => 'main_menu',
                'container' => false,
                'menu_class' =>'menu__list',
            ) );
            ?>
        </div>

    </div>
</header>