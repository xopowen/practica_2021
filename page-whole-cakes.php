<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title><?php echo   $wp_query->post->post_title;  ?></title>
    <?php wp_enqueue_script("jquery"); ?>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script src="https://cdn.polyfill.io/v2/polyfill.js?features=Element.prototype.closest"></script>
    <?php wp_head();?>
</head>
<body>
<?php get_header()?>
    <div class="heading">
        <div class="container">
            <div class="heading__body">
                <div class="heading__contant ">
                <h1 class="heading__h1 head_big_text"><?php echo   $wp_query->post->post_title;  ?></h1>
                    <div class="heading__way"> <p> <?php echo get_the_title(  get_page_by_path( 'main' ) ); ?> >  <?php echo   $wp_query->post->post_title;  ?> </p></div>
            </div>
            </div>

        </div>
    </div>

<div class="container">
    <div class="products">
        <div class="description">
            <div class="describe__body">
<?php
//выводим текст в рамке 
$args=array(
    'post_type'     => 'text_pole',
    'category_name' => 'text_on_page_cakes'
);
//запрос постов 
$whole_cake = query_posts($args);
foreach ($whole_cake as $post) : ?>
    <?php if(get_the_title() == 'тексе на странице тортов' ):?>
                    <p class="describe__text "><?php echo get_the_content(); ?>
                    </p>
    <?php else:  ?>
                    <div class="describe__Tilda"></div>
                    <h3><?php the_title()?></h3>
                    <p class="describe__text"><?php echo get_the_content(); ?></p>
    <?php endif;?>
<?php endforeach; 
?>
            </div>
        </div>
        
    <div class="products__body">
        <div class="card__top">

        </div>
        <?php
  $args = array(
    'post_type' => 'whole_cake',
    'showposts'=> 2,

  );
  
  $whole_cake = query_posts($args);
  global $whole_cake;
  foreach ($whole_cake as $post) :
  
  setup_postdata($post);
  
  $thumbnail_attributes = wp_get_attachment_image_src( get_post_thumbnail_id(), 'medium' );
  
  $custom_field_keys = get_post_custom_keys();
  
  $page_id = get_the_ID();
  
  $card__message ='';
  $card__Weight = '';
  $card__Price = '';
  
  foreach ( $custom_field_keys as $key => $value ) {
      $valuet = trim($value);
          if ( '_' == $valuet{0} )
              continue;
              
      switch($valuet){
          case "card__message":
              $card__message = get_post_meta($page_id, $value, true);
              break;
          case "card__Weight":
              $card__Weight = get_post_meta($page_id, $value, true);
              break;
          case "card__Price":
              $card__Price = get_post_meta($page_id, $value, true);
              break;
           }
      //echo $key .' => '. $value . '<br />';
  }
  ?>
    <div class="card">
    <div class="card__body">
        <div class="card__header">
            <button class="card__button">Заказать</button>
            <div class="card__hoverButton"></div>
            <picture><source srcset="<?php echo $thumbnail_attributes[0]; ?>" type="image/webp"><img src="<?php echo $thumbnail_attributes[0]; ?>" alt="" class="card__img"></picture>
            <picture><source srcset="<?php echo get_template_directory_uri(); ?>/assets/img/arrowleft.webp" type="image/webp"><img src="<?php echo $thumbnail_attributes[0];  ?>/assets/img/arrowleft.png" alt="" class="card__arrow card__arrow_right arrow"></picture>
            <picture><source srcset="<?php echo get_template_directory_uri(); ?>/assets/img/arrowleft.webp" type="image/webp"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/arrowleft.png" alt="" class="card__arrow  arrow"></picture>
        </div>

    <div class="card__main">
        <div class="card__heading">
            <h3>  <?php the_title(); ?></a></h3> <h2 class="card__message">
            <?php echo $card__message ;?></h2>
        </div>
        <div class="card__text">
            <p class="card__subtext">  <?php echo get_the_content(); ?></a>  </p>
        </div>
            <div class="card__fooder">
            <p class="card__WeightPrice"> <?php echo  $card__Weight ;?></p>
            <p class="card__WeightPrice"><?php echo $card__Price ;?> <span>у</span></p>
            </div>
        </div>
    </div>
</div>
     
<?php endforeach; 
?> 
    </div>
</div>
<?php
    global $wp_query;
 
// текущая страница
$paged = get_query_var( 'paged' ) ? get_query_var( 'paged' ) : 1;
// максимум страниц
$max_pages = $wp_query->max_num_pages;
 
// если текущая страница меньше, чем максимум страниц, то выводим кнопку
if( $paged < $max_pages ) {
    echo ('<div id="loadmore" style="text-align:center;">
    <a hidden  href="#" data-max_pages="' . $max_pages . '" data-paged="' . $paged . '" data-post_type = "wholeCake" class="button">Загрузить ещё</a>
</div>');
}
?>

    </div>
</div>

<?php get_footer();?>
<?php wp_footer();?>
</body>
</html>