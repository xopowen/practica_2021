<footer>
    <div class="menu">

        <div class="menu__body">
            <?php wp_nav_menu( array(
                'theme_location' => 'footer_menu',
                'container' => false,
                'menu_class' =>'menu__list',
            ) );
            ?>
        </div>
    </div>
    <div class="developer">
        <p class="developer__text">Разработчик :  </p><p class="developer__name">Xopowen</p>
    </div>
</footer>