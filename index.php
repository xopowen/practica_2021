 <!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Главная</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script src="https://api-maps.yandex.ru/2.1/?lang=ru_RU&amp;apikey=aae3f3e4-374f-420f-a948-677e1a58f577
" type="text/javascript"></script>
<?php wp_head();?>
</head>
<body>
<?php get_template_part( 'header_index' ); ?>
<main>
    <div class="face">
        <div class="face__img">
            <div class="face__blur"></div>
            <div class="face__frame">
                <picture><source srcset="<?php echo get_template_directory_uri(); ?>/assets/img/frame_top.webp" type="image/webp"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/frame_top.png" alt=""></picture>
            </div>
            <h1 class="face__slogan">MAKE WITH LOVE</h1>
            <div class="face__frame face__frame_button"></div>
        </div>
         <div class="container face__container">
            <div class="emblem">
                <picture><source srcset="<?php echo get_template_directory_uri(); ?>/assets/img/emblem__img.webp" type="image/webp"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/emblem__img.png" alt=""></picture>
            </div>
        </div>


    </div>


     <div class="container">
        <div class="products">
            <div class="inFrame">
                <div class="describe inFrame__describe">
                
<?php
//выводим текст в рамке 
$args=array(
    'post_type' => 'text_pole',
    'category_name'=> 'text_in_border_on_index'
);
//запрос постов 
$whole_cake = query_posts($args);
global $whole_cake;
foreach ($whole_cake as $post) :

setup_postdata($post);

$thumbnail_attributes = wp_get_attachment_image_src( get_post_thumbnail_id(), 'medium' );

?>
                    <p class="describe__text describe__text_center">
                            <?php echo get_the_content(); ?>
                    </p>

<?php endforeach; 
?>
                </div>
                <button type="submit" class="from__button inFrame__button"><p class="from__buttonText">Читать дальше</p></button>
            </div>
        </div>
        <div class="cakeCorners">
            <div class="cakeCornes__item cakeCornes__item_small">
                <picture class="cakeCorners__img"><source srcset="<?php echo get_template_directory_uri(); ?>/assets/img/cakeCornes/1.webp" type="image/webp"><picture><source srcset="<?php echo get_template_directory_uri(); ?>/assets/img/cakeCornes/1.webp" type="image/webp"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/cakeCornes/1.png" alt="" ></picture></picture>
                <div class="cakeCorners__describe">
                    <div class="cakeCorners__header">
                        <h3 class="cakeCorners__title">Торт «Красный бархат»</h3>
                        <p class="cakeCorners__text">Нежный бисквит пропитанный малиновым вареньем в сочетании со сливочным сыром</p>
                    </div>
                    <div class="cakeCorners__footer">
                        <p class="cakeCorners__WeightPrice">160 г.</p>
                        <p class="cakeCorners__WeightPrice">285 <span>у</span></p>
                    </div>
                </div>
            </div>
            <div class="cakeCornes__item cakeCornes__item_normall">
                <picture class="cakeCorners__img"><source srcset="<?php echo get_template_directory_uri(); ?>/assets/img/cakeCornes/2.webp" type="image/webp"><picture><source srcset="<?php echo get_template_directory_uri(); ?>/assets/img/cakeCornes/2.webp" type="image/webp"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/cakeCornes/2.png" alt="" ></picture></picture>
                <div class="cakeCorners__describe">
                    <div class="cakeCorners__header">
                        <h3 class="cakeCorners__title">Торт «Красный бархат»</h3>
                        <p class="cakeCorners__text">Нежный бисквит пропитанный малиновым вареньем в сочетании со сливочным сыром</p>
                    </div>
                    <div class="cakeCorners__footer">
                        <p class="cakeCorners__WeightPrice">160 г.</p>
                        <p class="cakeCorners__WeightPrice">285 <span>у</span></p>
                    </div>
                </div>
            </div>
            <div class="cakeCornes__item cakeCornes__item_big">
                <picture class="cakeCorners__img"><source srcset="<?php echo get_template_directory_uri(); ?>/assets/img/cakeCornes/3.webp" type="image/webp"><picture><source srcset="<?php echo get_template_directory_uri(); ?>/assets/img/cakeCornes/3.webp" type="image/webp"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/cakeCornes/3.png" alt="" ></picture></picture>
                <div class="cakeCorners__describe">
                    <div class="cakeCorners__header">
                        <h3 class="cakeCorners__title">Торт «Красный бархат»</h3>
                        <p class="cakeCorners__text">Нежный бисквит пропитанный малиновым вареньем в сочетании со сливочным сыром</p>
                    </div>
                    <div class="cakeCorners__footer">
                        <p class="cakeCorners__WeightPrice">160 г.</p>
                        <p class="cakeCorners__WeightPrice">285 <span>у</span></p>
                    </div>
                </div>
            </div>
            <div class="cakeCornes__item cakeCornes__item_normall">
                <picture class="cakeCorners__img"><source srcset="<?php echo get_template_directory_uri(); ?>/assets/img/cakeCornes/4.webp" type="image/webp"><picture><source srcset="<?php echo get_template_directory_uri(); ?>/assets/img/cakeCornes/4.webp" type="image/webp"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/cakeCornes/4.png" alt=""></picture></picture>
                <div class="cakeCorners__describe">
                    <div class="cakeCorners__header">
                      <h3 class="cakeCorners__title">Торт «Красный бархат»</h3>
                        <p class="cakeCorners__text">Нежный бисквит пропитанный малиновым вареньем в сочетании со сливочным сыром</p>
                    </div>
                    <div class="cakeCorners__footer">
                        <p class="cakeCorners__WeightPrice">160 г.</p>
                        <p class="cakeCorners__WeightPrice">285 <span>у</span></p>
                    </div>
                </div>
            </div>
            <div class="cakeCornes__item cakeCornes__item_small">
                <picture class="cakeCorners__img"><source srcset="<?php echo get_template_directory_uri(); ?>/assets/img/cakeCornes/5.webp" type="image/webp"><picture><source srcset="<?php echo get_template_directory_uri(); ?>/assets/img/cakeCornes/5.webp" type="image/webp"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/cakeCornes/5.png" alt="" ></picture></picture>
                <div class="cakeCorners__describe">
                    <div class="cakeCorners__header">
                        <h3 class="cakeCorners__title">Торт «Красный бархат»</h3>
                        <p class="cakeCorners__text">Нежный бисквит пропитанный малиновым вареньем в сочетании со сливочным сыром</p>
                    </div>
                    <div class="cakeCorners__footer">
                        <p class="cakeCorners__WeightPrice">160 г.</p>
                        <p class="cakeCorners__WeightPrice">285 <span>у</span></p>
                    </div>
                </div>
                </div>
        </div>

        <div class="contant wrap">
        <?php
//выводим посты приимущесть 
$args=array(
    'post_type' => 'advantage',
    'category_name'=> 'advantage_in_index'
);
//запрос постов 
$advantage_in_index = query_posts($args);
foreach ($advantage_in_index as $post) :

setup_postdata($post);

$thumbnail_attributes = wp_get_attachment_image_src( get_post_thumbnail_id(), 'medium' );

?>

            <div class="advantage">

                <picture><source srcset="<?php echo $thumbnail_attributes[0]; ?>" type="image/webp"><img src="<?php echo $thumbnail_attributes[0]; ?>" alt="" class="advantage__img advantage__img_big"></picture>
                <h2 class="advantage__head">  <?php the_title(); ?></h2>
                <p class="advantage__text">
                    <?php echo get_the_content(); ?>
                </p>
            </div>


<?php endforeach; 
?> 
        </div>

        <div class="slider">
             <ul class="slider__list">
                <?php
                    global $post;
                    $first_slait = 0; 
                    $args = array( 'numberposts' => -1, 'post_type' => 'slider');
                    $myposts = get_posts( $args );
                    foreach($myposts as $post){ setup_postdata($post); 
                        if($first_slait < 1):
                            $first_slait++;?>
                            <li class="slider__element slider__element_active"><?php the_post_thumbnail();?></li>
                       <?php else: ?> 

                            <li class= "slider__element "><?php the_post_thumbnail();?></li>
                
                        <?php 
                        endif;
                    };
                    wp_reset_postdata();
                    ?>
             </ul>
             <div class="slider__arrow slider__arrow_left"></div>
             <div class="slider__arrow slider__arrow_right"></div>
             <div class="slider__dots"></div>
        </div>
        <div class="heading">
                <div class="container">
                    <div class="heading__body">
                        <div class="heading__contant ">
                            <h1 class="heading__h1 head_big_text">контакты</h1>
                        </div>
                    </div>

            </div>
        </div>

    </div>
    <div class="contant container_below">
        <div class="ourData">
                <div class="ourDate__body">
                    <div class="ourDate__item">
                        <p class="ourDate__text">г. Иркутск, </p>
                        <p class="ourDate__text">ул. Карла Маркса, 26А</p>
                    </div>
                    <div class="ourDate__item">
                        <p class="ourDate__text ourDate__text_red">телефон:</p>
                        <p class="ourDate__text">8 (395) 276-71-00</p>
                    </div>
                    <div class="ourDate__item">
                        <p class="ourDate__text ourDate__text_red">email:</p>
                        <p class="ourDate__text">info@cc-union.ru</p>
                    </div>
                    <div class="ourDate__item">
                        <p class="ourDate__text ourDate__text_red">Мы в социальных сетях:</p>

                         <div class="ourDate__icons">
                             <p><a href=""><picture><source srcset="<?php echo get_template_directory_uri(); ?>/assets/img/icons/icon__telegram.webp" type="image/webp"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/icons/icon__telegram.png" alt="" class="ourDate__icon"></picture></a></p>
                             <p><a href=""><picture><source srcset="<?php echo get_template_directory_uri(); ?>/assets/img/icons/icon__telegram.webp" type="image/webp"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/icons/icon__telegram.png" alt="" class="ourDate__icon"></picture></a></p>
                         </div>

                    </div>
                </div>


        </div>
        <div id="map"></div>
    </div>

</main>

<?php get_footer();?>
<?php wp_footer();?>
</body>
</html>