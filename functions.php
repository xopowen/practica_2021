<?php
//включить миниатюры к постам 
add_theme_support( 'post-thumbnails' );



add_action( 'wp_enqueue_scripts', 'add_scpipts_and_style');
add_theme_support( 'custom-logo' );

//подключение стилей и скриптов 
function add_scpipts_and_style(){
    wp_enqueue_script( 'JavaScript',get_template_directory_uri().'/assets/js/JavaScript.js'
    ,false,null,true);
    $id = is_page();
    if(is_page( sanitize_title('menu'))){  
            wp_enqueue_script( 'JavaScript_menu',get_template_directory_uri().'/assets/js/JavaScript_menu.js'
            ,false,null,true);
            wp_enqueue_script( 'JavaScript_pacen',get_template_directory_uri().'/assets/js/JavaScript_pacen.js'
            ,false,null,true);
        }elseif(is_page( sanitize_title('contacts'))){
            wp_enqueue_script( 'JavaScript_map',get_template_directory_uri().'/assets/js/JavaScript_map.js'
            ,false,null,true);
        }elseif(is_page( sanitize_title('whole-cakes'))){
            wp_enqueue_script( 'JavaScript_map',get_template_directory_uri().'/assets/js/JavaScript_cakes.js'
            ,false,null,true);
            wp_enqueue_script( 'JavaScript_pacen',get_template_directory_uri().'/assets/js/JavaScript_pacen.js'
            ,false,null,true);
        }elseif(is_page( sanitize_title('loyalty-program'))){

        }else{
            wp_enqueue_script( 'JavaScript_index',get_template_directory_uri().'/assets/js/JavaScript_index.js'
            ,false,null,true);
            wp_enqueue_script( 'JavaScript_map',get_template_directory_uri().'/assets/js/JavaScript_map.js'
            ,false,null,true);
    }

    wp_enqueue_style( 'style',get_stylesheet_uri() ); 
};  




add_filter( 'get_custom_logo', 'change_logo_class' );

function change_logo_class( $html ) {

    $html = str_replace( 'custom-logo', '', $html );
    $html = str_replace( 'custom-logo-link', 'your-custom-class', $html );
    return $html;
}


//добовления доболнительного поля настройки вывода логотипа
function my_customize_register( $wp_customize ) {
    $wp_customize->add_setting('header_logo', array(
        'default' => '',
        'sanitize_callback' => 'absint',
    ));

    $wp_customize->add_control(new WP_Customize_Media_Control($wp_customize, 'header_logo', array(
        'section' => 'title_tagline',
        'label' => 'Логотип на главную страницу'
    )));

    $wp_customize->selective_refresh->add_partial('header_logo', array(
        'selector' => '.header-logo',
        'render_callback' => function() {
            $logo = get_theme_mod('header_logo');
            $img = wp_get_attachment_image_src($logo, 'full');
            if ($img) {
                return '<img  src="' . $img[0] . '" alt="" >';
            } else {
                return '';
            }
        }
    ));
}
add_action( 'customize_register', 'my_customize_register' );

//регестрация меню 
register_nav_menus(
    array(
        'header_menu' => esc_html__( 'header_menu','' ),
        'topbar_menu' => esc_html__( 'Top Bar', 'oceanwp' ),
        'main_menu'   => esc_html__( 'Main', 'oceanwp' ),
        'footer_menu' => esc_html__( 'footer_menu', '' ),
        'mobile_menu' => esc_html__( 'Mobile (optional)', 'oceanwp' ),
    )
);

//хук для вставки логотипа в середину  меню 
add_filter( 'nav_menu_css_class', '__return_empty_array' );
add_filter( 'wp_nav_menu_items', 'your_custom_menu_item', 10, 2 );
function your_custom_menu_item ( $items, $args ) {
    $header_logo = get_theme_mod('header_logo');
    $img = wp_get_attachment_image_src($header_logo, 'full');
    $array = explode('</li>', $items);
    $items = '';
    if ($args->theme_location == 'header_menu') {
       // echo substr_count ($items ,'</li>');
        for ($i = 0; $i < count($array); $i++){
            if($i != round(count($array)/2,0, PHP_ROUND_HALF_DOWN)){
                $items.= $array[$i].'<div class="menu__underline"></div></li>';
            }else{
                if ($img) {
                $items .= '<div class="logo">
                <picture class="logo__img"><source srcset="'.get_template_directory_uri( ).'/assets/img/logo2.webp" type="image/webp"><a href="/" class="header-logo">
                        <img src="'. $img[0].'" alt="">
                        </a></picture> 
            </div>' . $array[$i].'<div class="menu__underline"></div></li>';
                }else{
                    $items .= $items[$i].'<div class="menu__underline"></div></li>';
                }
        }
        }            
    }elseif($args->theme_location == 'footer_menu'){
        for ($i = 0; $i < count($array); $i++){
            if($i != round(count($array)/2,0, PHP_ROUND_HALF_DOWN)){
                $items.= $array[$i].'<div class="menu__underline"></div></li>';
            }else{
                if ($img) {
                $items .= '<div class="logo">
                        <picture><source srcset="'.get_template_directory_uri().'/assets/img/icons/vk.webp" type="image/webp"><img class="messenger" src="'.get_template_directory_uri().'/assets/img/icons/vk.png"></picture>
                        <picture class="logo__img logo__img_footer">'.get_custom_logo( ).'</picture>
                        <picture><source srcset="'.get_template_directory_uri().'/assets/img/icons/telegram.webp" type="image/webp"><img class="messenger" src="'.get_template_directory_uri().'/assets/img/icons/telegram.png"></picture>
                        
            </div>' . $array[$i].'<div class="menu__underline"></div></li>';
                }else{
                    //$items .= $items[$i].'<div class="menu__underline"></div></li>';
                }
        }
        } 
    }elseif($args->theme_location == 'main_menu'){
        for ($i = 0; $i < count($array); $i++){
            if($i != round(count($array)/2,0, PHP_ROUND_HALF_DOWN)){
                $items.= $array[$i].'<div class="menu__underline"></div></li>';
            }else{
                if ($img) {
                $items .= '<div class="logo">
                <picture class="logo__img">'.get_custom_logo(  ).'</picture>
            </div>' . $array[$i].'<div class="menu__underline"></div></li>';
                }else{
                    $items .= $items[$i].'<div class="menu__underline"></div></li>';
                }
        }
        }    
    }

    return $items;
}

//создаём глобальную ссылку на функцию обработки ajaxзапросов
function js_variables(){
    $variables = array (
        'ajax_url' => admin_url('admin-ajax.php'),
        'is_mobile' => wp_is_mobile()
        // Тут обычно какие-то другие переменные
    );
    echo(
        '<script type="text/javascript">window.wp_data = '.
        json_encode($variables) .
        ';</script>'
    );
}
add_action('wp_head','js_variables');




//ajax загруспа постов по категориям 
add_action( 'wp_ajax_categorie', 'pacen_products_categorie' );
add_action( 'wp_ajax_nopriv_categorie', 'pacen_products_categorie' );
 
function pacen_products_categorie() {
	$args = array(
        'post_type' 	 => 'products',
        'cat'=> $_POST[ 'categories' ],
		'post_status' => 'publish'
	);

  $MY_QUERY = new WP_Query( $args );

    if ( $MY_QUERY->have_posts() ) :
        while ( $MY_QUERY->have_posts() ) : $MY_QUERY->the_post();
        setup_postdata($post);
        $thumbnail_attributes = wp_get_attachment_image_src( get_post_thumbnail_id(), 'medium' );
        $custom_field_keys = get_post_custom_keys();
        $page_id = get_the_ID();
        $card__message ='';
        $card__Weight = '';
        $card__Price = '';
    foreach ( $custom_field_keys as $key => $value ) {
        $valuet = trim($value);
            if ( '_' == $valuet{0} )
                continue;
                
        switch($valuet){
            case "card__message":
                $card__message = get_post_meta($page_id, $value, true);
                break;
            case "card__Weight":
                $card__Weight = get_post_meta($page_id, $value, true);
                break;
            case "card__Price":
                $card__Price = get_post_meta($page_id, $value, true);
                break;
             }
    }
    ?>
        <div class="card">
        <div class="card__body">
            <div class="card__header">
                <picture><source srcset="<?php echo $thumbnail_attributes[0]; ?>" type="image/webp"><img src="<?php echo $thumbnail_attributes[0]; ?>" alt="" class="card__img"></picture>
            </div>
    
        <div class="card__main">
            <div class="card__heading">
                <h3>  <?php the_title(); ?></a></h3> <h2 class="card__message">
                <?php echo $card__message ;?></h2>
            </div>
            <div class="card__text">
                <p class="card__subtext">  <?php echo get_the_content(); ?></a>  </p>
            </div>
                <div class="card__fooder">
                <p class="card__WeightPrice"> <?php echo  $card__Weight ;?></p>
                <p class="card__WeightPrice"><?php echo $card__Price ;?> <span>у</span></p>
                </div>
            </div>
        </div>
    </div>
    <?php 
	    endwhile;
    endif;
    die;
 
};




//ajax пагенация меню
add_action( 'wp_ajax_products', 'pacen_products' );
add_action( 'wp_ajax_nopriv_products', 'pacen_products' );
 
function pacen_products() {
 
	$paged = ! empty( $_POST[ 'paged' ] ) ? $_POST[ 'paged' ] : 1;
	$paged++;
 
	$args = array(
        'posts_per_page' => 3,
        'post_type' 	 => 'products',
        'paged'          => $paged,
        'cat'=> $_POST[ 'categorie' ],
		'post_status' => 'publish'
	);

  $MY_QUERY = new WP_Query( $args );

    if ( $MY_QUERY->have_posts() ) :
        while ( $MY_QUERY->have_posts() ) : $MY_QUERY->the_post();
        setup_postdata($post);
        $thumbnail_attributes = wp_get_attachment_image_src( get_post_thumbnail_id(), 'medium' );
        $custom_field_keys = get_post_custom_keys();
        $page_id = get_the_ID();
        $card__message ='';
        $card__Weight = '';
        $card__Price = '';
    foreach ( $custom_field_keys as $key => $value ) {
        $valuet = trim($value);
            if ( '_' == $valuet{0} )
                continue;
                
        switch($valuet){
            case "card__message":
                $card__message = get_post_meta($page_id, $value, true);
                break;
            case "card__Weight":
                $card__Weight = get_post_meta($page_id, $value, true);
                break;
            case "card__Price":
                $card__Price = get_post_meta($page_id, $value, true);
                break;
             }
    }
    ?>
        <div class="card">
        <div class="card__body">
            <div class="card__header">
                <picture><source srcset="<?php echo $thumbnail_attributes[0]; ?>" type="image/webp"><img src="<?php echo $thumbnail_attributes[0]; ?>" alt="" class="card__img"></picture>
            </div>
    
        <div class="card__main">
            <div class="card__heading">
                <h3>  <?php the_title(); ?></a></h3> <h2 class="card__message">
                <?php echo $card__message ;?></h2>
            </div>
            <div class="card__text">
                <p class="card__subtext">  <?php echo get_the_content(); ?></a>  </p>
            </div>
                <div class="card__fooder">
                <p class="card__WeightPrice"> <?php echo  $card__Weight ;?></p>
                <p class="card__WeightPrice"><?php echo $card__Price ;?> <span>у</span></p>
                </div>
            </div>
        </div>
    </div>
<?php 
	    endwhile;
    endif;
    die;
 
};


//ajax пагенация цельные торты 
add_action( 'wp_ajax_wholeCake', 'pacen_whole_cake' );
add_action( 'wp_ajax_nopriv_wholeCake', 'pacen_whole_cake' );
 
function pacen_whole_cake() {
 
	$paged = ! empty( $_POST[ 'paged' ] ) ? $_POST[ 'paged' ] : 1;
	$paged++;
 
	$args = array(
        'posts_per_page' => 2,
        'post_type' 	 => 'whole_cake',
		'paged'          => $paged,
		'post_status'    => 'publish'
	);

  $MY_QUERY = new WP_Query( $args );

    if ( $MY_QUERY->have_posts() ) :
        while ( $MY_QUERY->have_posts() ) : $MY_QUERY->the_post();
        setup_postdata($post);
        $thumbnail_attributes = wp_get_attachment_image_src( get_post_thumbnail_id(), 'medium' );
        $custom_field_keys = get_post_custom_keys();
        $page_id = get_the_ID();
        $card__message ='';
        $card__Weight = '';
        $card__Price = '';
    foreach ( $custom_field_keys as $key => $value ) {
        $valuet = trim($value);
            if ( '_' == $valuet{0} )
                continue;
                
        switch($valuet){
            case "card__message":
                $card__message = get_post_meta($page_id, $value, true);
                break;
            case "card__Weight":
                $card__Weight = get_post_meta($page_id, $value, true);
                break;
            case "card__Price":
                $card__Price = get_post_meta($page_id, $value, true);
                break;
             }
    }
    ?>
    <div class="card">
    <div class="card__body">
        <div class="card__header">
            <button class="card__button">Заказать</button>
            <div class="card__hoverButton"></div>
            <picture><source srcset="<?php echo $thumbnail_attributes[0]; ?>" type="image/webp"><img src="<?php echo $thumbnail_attributes[0]; ?>" alt="" class="card__img"></picture>
            <picture><source srcset="<?php echo get_template_directory_uri(); ?>/assets/img/arrowleft.webp" type="image/webp"><img src="<?php echo $thumbnail_attributes[0];  ?>/assets/img/arrowleft.png" alt="" class="card__arrow card__arrow_right arrow"></picture>
            <picture><source srcset="<?php echo get_template_directory_uri(); ?>/assets/img/arrowleft.webp" type="image/webp"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/arrowleft.png" alt="" class="card__arrow  arrow"></picture>
        </div>

    <div class="card__main">
        <div class="card__heading">
            <h3>  <?php the_title(); ?></a></h3> <h2 class="card__message">
            <?php echo $card__message ;?></h2>
        </div>
        <div class="card__text">
            <p class="card__subtext">  <?php echo get_the_content(); ?></a>  </p>
        </div>
            <div class="card__fooder">
            <p class="card__WeightPrice"> <?php echo  $card__Weight ;?></p>
            <p class="card__WeightPrice"><?php echo $card__Price ;?> <span>у</span></p>
            </div>
        </div>
    </div>
</div>
<?php 
	    endwhile;
    endif;
    die;
 
};


//добовляем поле админ панел без плагена
add_action( 'init', 'register_group_post_type' );
function register_group_post_type() {
/**
	 * Post Type: Текст на сайте .
	 */

	$text = [
		"name" => __( "Текст на сайте ", "custom-post-type-ui" ),
		"singular_name" => __( "текст ", "custom-post-type-ui" ),
		"menu_name" => __( "Мой Текст на сайте", "custom-post-type-ui" ),
		"all_items" => __( "Все Тексты на сайте", "custom-post-type-ui" ),
		"add_new" => __( "добавить новый", "custom-post-type-ui" ),
		"add_new_item" => __( "добавить новый текст", "custom-post-type-ui" ),
		"edit_item" => __( "редактировать текст", "custom-post-type-ui" ),
		"new_item" => __( "Новый текст", "custom-post-type-ui" ),
		"view_item" => __( "Просмотреть текст", "custom-post-type-ui" ),
		"view_items" => __( "Просмотреть Текст на сайте", "custom-post-type-ui" ),
		"search_items" => __( "Искать Текст на сайте", "custom-post-type-ui" ),
		"not_found" => __( "не обнаружена Текста на сайте", "custom-post-type-ui" ),
		"not_found_in_trash" => __( "не найдено в корзине", "custom-post-type-ui" ),
		"parent" => __( "родительский текст :", "custom-post-type-ui" ),
		"featured_image" => __( "Популярное изображение текст", "custom-post-type-ui" ),
		"set_featured_image" => __( "Установить избранное изображение тексту", "custom-post-type-ui" ),
		"remove_featured_image" => __( "Удалить избранное изображение", "custom-post-type-ui" ),
		"use_featured_image" => __( "Использовать избранное изображение", "custom-post-type-ui" ),
		"archives" => __( "текст  Архивы", "custom-post-type-ui" ),
		"insert_into_item" => __( "Вставить в элемент", "custom-post-type-ui" ),
		"uploaded_to_this_item" => __( "Загружено в этот элемент", "custom-post-type-ui" ),
		"filter_items_list" => __( "Список элементов фильтра", "custom-post-type-ui" ),
		"items_list_navigation" => __( "Навигация по списку предметов", "custom-post-type-ui" ),
		"items_list" => __( "Текст на сайте  Список предметов", "custom-post-type-ui" ),
		"attributes" => __( "Атрибуты", "custom-post-type-ui" ),
		"name_admin_bar" => __( "Текст  Новое", "custom-post-type-ui" ),
		"item_published" => __( "текст  опубликован", "custom-post-type-ui" ),
		"item_published_privately" => __( "текст  опубликован", "custom-post-type-ui" ),
		"item_reverted_to_draft" => __( "текст  возвращен в черновик", "custom-post-type-ui" ),
		"item_scheduled" => __( "текст  запланирован", "custom-post-type-ui" ),
		"item_updated" => __( "текст  обновлен.", "custom-post-type-ui" ),
		"parent_item_colon" => __( "родительский текст :", "custom-post-type-ui" ),
	];

	$args = [
		"label" => __( "Текст на сайте ", "custom-post-type-ui" ),
		"labels" => $text,
		"description" => "",
		"public" => true,
		"publicly_queryable" => false,
		"show_ui" => true,
		"show_in_rest" => true,
		"rest_base" => "",
		"rest_controller_class" => "WP_REST_Posts_Controller",
		"has_archive" => false,
		"show_in_menu" => true,
		"show_in_nav_menus" => true,
		"delete_with_user" => false,
		"exclude_from_search" => false,
		"capability_type" => "post",
		"map_meta_cap" => true,
		"hierarchical" => false,
		"rewrite" => [ "slug" => "text_pole", "with_front" => true ],
		"query_var" => true,
        "menu_position" => 1,
		"supports" => [ "title", "editor", "thumbnail", "excerpt", "revisions", "page-attributes", "post-formats" ],
		"taxonomies" => [ "category" ],
		"show_in_graphql" => false,
	];

	register_post_type( "text_pole", $args );
  // Слайдер - тип записи
  register_post_type('slider', array(
    'label'               => 'Слайдер',
    'labels'              => array(
      'name'          => 'Слайдер',
      'singular_name' => 'Слайдер',
      'menu_name'     => 'Слайдер',
      'all_items'     => 'Все слайды',
      'add_new'       => 'Добавить слайд',
      'add_new_item'  => 'Добавить новый слайд',
      'edit'          => 'Редактировать',
      'edit_item'     => 'Редактировать слайд',
      'new_item'      => 'Новый слайд',
    ),
    'description'         => 'Слайдер для главной страницы',
    'public'              => true,
    'publicly_queryable'  => true,
    'show_ui'             => true,
    'show_in_rest'        => false,
    'rest_base'           => '',
    'show_in_menu'        => true,
    'exclude_from_search' => false,
    'capability_type'     => 'post',
    'map_meta_cap'        => true,
    'menu_icon'           => 'dashicons-format-image',
    'hierarchical'        => false,
    'rewrite'             => false,
    'has_archive'         => false,
    'query_var'           => true,
    'supports'            => array( 'title', 'thumbnail' ),
  ) );
}

function cptui_register_my_cpts() {

	/**
	 * Post Type: Карточки тортов.
	 */

	$labels = [
		"name" => __( "Карточки тортов", "custom-post-type-ui" ),
		"singular_name" => __( "карточка тортов", "custom-post-type-ui" ),
		"menu_name" => __( "My Карточки тортов", "custom-post-type-ui" ),
		"all_items" => __( "All Карточки тортов", "custom-post-type-ui" ),
		"add_new" => __( "Add new", "custom-post-type-ui" ),
		"add_new_item" => __( "Add new карточка тортов", "custom-post-type-ui" ),
		"edit_item" => __( "Edit карточка тортов", "custom-post-type-ui" ),
		"new_item" => __( "New карточка тортов", "custom-post-type-ui" ),
		"view_item" => __( "View карточка тортов", "custom-post-type-ui" ),
		"view_items" => __( "View Карточки тортов", "custom-post-type-ui" ),
		"search_items" => __( "Search Карточки тортов", "custom-post-type-ui" ),
		"not_found" => __( "No Карточки тортов found", "custom-post-type-ui" ),
		"not_found_in_trash" => __( "No Карточки тортов found in trash", "custom-post-type-ui" ),
		"parent" => __( "Parent карточка тортов:", "custom-post-type-ui" ),
		"featured_image" => __( "Featured image for this карточка тортов", "custom-post-type-ui" ),
		"set_featured_image" => __( "Set featured image for this карточка тортов", "custom-post-type-ui" ),
		"remove_featured_image" => __( "Remove featured image for this карточка тортов", "custom-post-type-ui" ),
		"use_featured_image" => __( "Use as featured image for this карточка тортов", "custom-post-type-ui" ),
		"archives" => __( "карточка тортов archives", "custom-post-type-ui" ),
		"insert_into_item" => __( "Insert into карточка тортов", "custom-post-type-ui" ),
		"uploaded_to_this_item" => __( "Upload to this карточка тортов", "custom-post-type-ui" ),
		"filter_items_list" => __( "Filter Карточки тортов list", "custom-post-type-ui" ),
		"items_list_navigation" => __( "Карточки тортов list navigation", "custom-post-type-ui" ),
		"items_list" => __( "Карточки тортов list", "custom-post-type-ui" ),
		"attributes" => __( "Карточки тортов attributes", "custom-post-type-ui" ),
		"name_admin_bar" => __( "карточка тортов", "custom-post-type-ui" ),
		"item_published" => __( "карточка тортов published", "custom-post-type-ui" ),
		"item_published_privately" => __( "карточка тортов published privately.", "custom-post-type-ui" ),
		"item_reverted_to_draft" => __( "карточка тортов reverted to draft.", "custom-post-type-ui" ),
		"item_scheduled" => __( "карточка тортов scheduled", "custom-post-type-ui" ),
		"item_updated" => __( "карточка тортов updated.", "custom-post-type-ui" ),
		"parent_item_colon" => __( "Parent карточка тортов:", "custom-post-type-ui" ),
	];

	$args = [
		"label" => __( "Карточки тортов", "custom-post-type-ui" ),
		"labels" => $labels,
		"description" => "",
		"public" => true,
		"publicly_queryable" => false,
		"show_ui" => true,
		"show_in_rest" => true,
		"rest_base" => "cakes",
		"rest_controller_class" => "Whole_cakes",
		"has_archive" => false,
		"show_in_menu" => true,
		"show_in_nav_menus" => true,
		"delete_with_user" => false,
		"exclude_from_search" => false,
		"capability_type" => "post",
		"map_meta_cap" => true,
		"hierarchical" => false,
		"rewrite" => [ "slug" => "название торта", "with_front" => true ],
		"query_var" => true,
		"menu_position" => 4,
		"supports" => [ "title", "editor", "thumbnail", "excerpt", "trackbacks", "custom-fields", "comments", "revisions", "author", "page-attributes", "post-formats" ],
		"taxonomies" => [ "category", "post_tag" ],
		"show_in_graphql" => false,
	];

	register_post_type( "whole_cake", $args );

	/**
	 * Post Type: Товары.
	 */

	$labels = [
		"name" => __( "Товары", "custom-post-type-ui" ),
		"singular_name" => __( "товар", "custom-post-type-ui" ),
		"menu_name" => __( "Мои  Товары", "custom-post-type-ui" ),
		"all_items" => __( "Все Товары", "custom-post-type-ui" ),
		"add_new" => __( "Новый товар", "custom-post-type-ui" ),
		"add_new_item" => __( "добавить новый товар", "custom-post-type-ui" ),
		"edit_item" => __( "Редактировать товар", "custom-post-type-ui" ),
		"new_item" => __( "Новый товар", "custom-post-type-ui" ),
		"view_item" => __( "Просмотреть товар", "custom-post-type-ui" ),
		"view_items" => __( "Просмотр Товары", "custom-post-type-ui" ),
		"search_items" => __( "Искать товар", "custom-post-type-ui" ),
		"not_found" => __( "Товары не обнаружены", "custom-post-type-ui" ),
		"not_found_in_trash" => __( "Товары не обнаружены в карзине", "custom-post-type-ui" ),
		"parent" => __( "Родительский товар:", "custom-post-type-ui" ),
		"featured_image" => __( "Популярное изображение товара", "custom-post-type-ui" ),
		"set_featured_image" => __( "Установить избранное изображение товара", "custom-post-type-ui" ),
		"remove_featured_image" => __( "Удалить избранное изображение товара", "custom-post-type-ui" ),
		"use_featured_image" => __( "Использовать избранное изображение товара", "custom-post-type-ui" ),
		"archives" => __( "Архивы товаров", "custom-post-type-ui" ),
		"insert_into_item" => __( "Вставить в товар", "custom-post-type-ui" ),
		"uploaded_to_this_item" => __( "Загружено в  товар", "custom-post-type-ui" ),
		"filter_items_list" => __( "Список Товаров", "custom-post-type-ui" ),
		"items_list_navigation" => __( "Товары Навигация по списку", "custom-post-type-ui" ),
		"items_list" => __( "Товары Список", "custom-post-type-ui" ),
		"attributes" => __( "Товары Атрибуты", "custom-post-type-ui" ),
		"name_admin_bar" => __( "«Новое» товар", "custom-post-type-ui" ),
		"item_published" => __( "товар опубликован", "custom-post-type-ui" ),
		"item_published_privately" => __( "товар опубликован в частном порядке", "custom-post-type-ui" ),
		"item_reverted_to_draft" => __( "Товар возвращен в черновик", "custom-post-type-ui" ),
		"item_scheduled" => __( "Товар запланирован", "custom-post-type-ui" ),
		"item_updated" => __( "Товар обновлен", "custom-post-type-ui" ),
		"parent_item_colon" => __( "Родительский товар:", "custom-post-type-ui" ),
	];

	$args = [
		"label" => __( "Товары", "custom-post-type-ui" ),
		"labels" => $labels,
		"description" => "",
		"public" => true,
		"publicly_queryable" => false,
		"show_ui" => true,
		"show_in_rest" => true,
		"rest_base" => "",
		"rest_controller_class" => "WP_REST_Posts_Controller",
		"has_archive" => false,
		"show_in_menu" => true,
		"show_in_nav_menus" => true,
		"delete_with_user" => false,
		"exclude_from_search" => false,
		"capability_type" => "post",
		"map_meta_cap" => true,
		"hierarchical" => false,
		"rewrite" => [ "slug" => "products", "with_front" => true ],
		"query_var" => true,
		"menu_position" => 5,
		"supports" => [ "title", "editor", "thumbnail", "excerpt", "trackbacks", "custom-fields", "comments", "revisions", "author", "page-attributes", "post-formats" ],
		"taxonomies" => [ "category" ],
		"show_in_graphql" => false,
	];

	register_post_type( "products", $args );

	/**
	 * Post Type: Преимущества нашей пекарни.
	 */

	$labels = [
		"name" => __( "Преимущества нашей пекарни", "custom-post-type-ui" ),
		"singular_name" => __( "Преимущество нашей пекарни", "custom-post-type-ui" ),
		"menu_name" => __( "Преимущества нашей пекарни", "custom-post-type-ui" ),
		"all_items" => __( "Все Преимущества нашей пекарни", "custom-post-type-ui" ),
		"add_new" => __( "добавить новое", "custom-post-type-ui" ),
		"add_new_item" => __( "добавить новое Преимущество нашей пекарни", "custom-post-type-ui" ),
		"edit_item" => __( "редактировать Преимущество нашей пекарни", "custom-post-type-ui" ),
		"new_item" => __( "новое Преимущество нашей пекарни", "custom-post-type-ui" ),
		"view_item" => __( "Просмотр Преимущество нашей пекарни", "custom-post-type-ui" ),
		"view_items" => __( "Просмотр Преимущества нашей пекарни", "custom-post-type-ui" ),
		"search_items" => __( "Искать Преимущества нашей пекарни", "custom-post-type-ui" ),
		"not_found" => __( "не обнаружено Преимущества нашей пекарни found", "custom-post-type-ui" ),
		"not_found_in_trash" => __( "не найдено Преимущества нашей пекарни found in trash", "custom-post-type-ui" ),
		"parent" => __( "родитель Преимущество нашей пекарни:", "custom-post-type-ui" ),
		"featured_image" => __( "популярное сообщение Преимущество нашей пекарни", "custom-post-type-ui" ),
		"set_featured_image" => __( "установить изображение Преимущество нашей пекарни", "custom-post-type-ui" ),
		"remove_featured_image" => __( "удалить изображение Преимущество нашей пекарни", "custom-post-type-ui" ),
		"use_featured_image" => __( "использовать избранное изображение", "custom-post-type-ui" ),
		"archives" => __( "Преимущество нашей пекарни архив", "custom-post-type-ui" ),
		"insert_into_item" => __( "вставить в элемент", "custom-post-type-ui" ),
		"uploaded_to_this_item" => __( "Загружено в этот элемент", "custom-post-type-ui" ),
		"filter_items_list" => __( "фильтр Преимущества нашей пекарни list", "custom-post-type-ui" ),
		"items_list_navigation" => __( "Преимущества нашей пекарни Навигация по списку", "custom-post-type-ui" ),
		"items_list" => __( "Преимущества нашей пекарни Список", "custom-post-type-ui" ),
		"attributes" => __( "Преимущества нашей пекарни Атрибуты", "custom-post-type-ui" ),
		"name_admin_bar" => __( "Новое Преимущество нашей пекарни", "custom-post-type-ui" ),
		"item_published" => __( "Преимущество нашей пекарни опубликовано", "custom-post-type-ui" ),
		"item_published_privately" => __( "Преимущество нашей пекарни опубликован в частном порядке", "custom-post-type-ui" ),
		"item_reverted_to_draft" => __( "Преимущество нашей пекарни возвращен в черновик", "custom-post-type-ui" ),
		"item_scheduled" => __( "Преимущество нашей пекарни запланирован", "custom-post-type-ui" ),
		"item_updated" => __( "Преимущество нашей пекарни обновлен.", "custom-post-type-ui" ),
		"parent_item_colon" => __( "родитель Преимущество нашей пекарни:", "custom-post-type-ui" ),
	];

	$args = [
		"label" => __( "Преимущества нашей пекарни", "custom-post-type-ui" ),
		"labels" => $labels,
		"description" => "",
		"public" => true,
		"publicly_queryable" => false,
		"show_ui" => true,
		"show_in_rest" => true,
		"rest_base" => "",
		"rest_controller_class" => "WP_REST_Posts_Controller",
		"has_archive" => false,
		"show_in_menu" => true,
		"show_in_nav_menus" => true,
		"delete_with_user" => false,
		"exclude_from_search" => false,
		"capability_type" => "post",
		"map_meta_cap" => true,
		"hierarchical" => false,
		"rewrite" => [ "slug" => "advantage", "with_front" => true ],
		"query_var" => true,
		"supports" => [ "title", "editor", "thumbnail", "custom-fields", "revisions" ],
		"taxonomies" => [ "category" ],
		"show_in_graphql" => false,
	];

	register_post_type( "advantage", $args );

	
}

add_action( 'init', 'cptui_register_my_cpts' );





?>