<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title><?php echo   $wp_query->post->post_title;  ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php wp_head();?>
</head>
<body>
<main>
    <div class="container">
        <div class="centered">
        <div class="error">
            <div class="error__item"><p>4</p></div>
            <div class="error__item"><picture><source srcset="<?php echo get_template_directory_uri(); ?>/assets/img/img_in_error.webp" type="image/webp"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/img_in_error.png" alt=""></picture></div>
            <div class="error__item"><p>4</p></div>
        </div>
        <div class="info">
            <div class="info__item">
                <h2>УПС страницу кто-то съел</h2>
            </div>
            <div class="info__item"><picture><source srcset="<?php echo get_template_directory_uri(); ?>/assets/img/emoji_in_error.webp" type="image/webp"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/emoji_in_error.png" alt=""></picture></div>
        </div>
         <div class="navigation"><p class=" subtext subtext_error"><a href="<?php echo get_the_permalink(  get_page_by_path( 'menu' ));?>"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/vector_smart_object.png"> Назад в <?php echo get_the_title(  get_page_by_path( 'menu' ) ); ?></a></p></div>
        <p class="subtext ">Но вы всегда можете связаться с нами по контактам ниже.</p>
        <div class="contant">
            <div class="Contacts">
                <div class="Contacts__item">
                    <p class="Contacts__address">Иркутск, Карла Маркса, 26а</p>
                </div>
                <div class="Contacts__item">
                    <p><a href=""><picture><source srcset="<?php echo get_template_directory_uri(); ?>/assets/img/icons/icon__phone.webp" type="image/webp"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/icons/icon__phone.png" alt="" class="Contacts__icon"></picture></a></p>
                    <p class="Contacts__phone">+7 (3952) 76‒71‒99</p>
                </div>
                <div class="Contacts__item">
                    <p><a href=""><picture><source srcset="<?php echo get_template_directory_uri(); ?>/assets/img/icons/facebook.webp" type="image/webp"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/icons/facebook.png" alt="" class="Contacts__icon"></picture></a></p>
                    <p><a href=""><picture><source srcset="<?php echo get_template_directory_uri(); ?>/assets/img/icons/icon__vk.webp" type="image/webp"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/icons/icon__vk.png" alt="" class="Contacts__icon"></picture></a></p>
                    <p><a href=""><picture><source srcset="<?php echo get_template_directory_uri(); ?>/assets/img/icons/icon__telegram.webp" type="image/webp"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/icons/icon__telegram.png" alt="" class="Contacts__icon"></picture></a></p>
                </div>
            </div>
        </div>
    </div>
    </div>
</main>
</body>
</html>