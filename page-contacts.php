<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title><?php echo   $wp_query->post->post_title;  ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script src="https://api-maps.yandex.ru/2.1/?lang=ru_RU&amp;apikey=aae3f3e4-374f-420f-a948-677e1a58f577
" type="text/javascript"></script>
<?php wp_head();?>
</head>
<body>
<?php get_header()?>
    <div class="heading">
        <div class="container">
            <div class="heading__body">
                <div class="heading__contant ">
                <h1 class="heading__h1 head_big_text"><?php echo   $wp_query->post->post_title;  ?></h1>
                    <div class="heading__way"> <p><?php echo get_the_title(  get_page_by_path( 'main' ) ); ?> > <?php echo   $wp_query->post->post_title;  ?> </p>
                    </div>
                    <div class="navigation navigation__heading"><p class="card__subtext"><a href="<?php echo get_the_permalink(  get_page_by_path( 'menu' ));?>"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/vector_smart_object.png"> Назад в <?php echo get_the_title(  get_page_by_path( 'menu' ) ); ?></a></p></div>
            </div>
            </div>
        </div>
    </div>

<main>
    <div class="container ">
        <div class="contant">
            <div class="Contacts">
                <div class="Contacts__item">
                    <p class="Contacts__address">Иркутск, Карла Маркса, 26а</p>
                </div>
                <div class="Contacts__item">
                    <p><a href=""><picture><source srcset="<?php echo get_template_directory_uri(); ?>/assets/img/icons/icon__phone.webp" type="image/webp"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/icons/icon__phone.png" alt="" class="Contacts__icon"></picture></a></p>
                    <p class="Contacts__phone">+7 (3952) 76‒71‒99</p>
                </div>
                <div class="Contacts__item">
                    <p><a href=""><picture><source srcset="<?php echo get_template_directory_uri(); ?>/assets/img/icons/facebook.webp" type="image/webp"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/icons/facebook.png" alt="" class="Contacts__icon"></picture></a></p>
                    <p><a href=""><picture><source srcset="<?php echo get_template_directory_uri(); ?>/assets/img/icons/icon__vk.webp" type="image/webp"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/icons/icon__vk.png" alt="" class="Contacts__icon"></picture></a></p>
                    <p><a href=""><picture><source srcset="<?php echo get_template_directory_uri(); ?>/assets/img/icons/icon__telegram.webp" type="image/webp"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/icons/icon__telegram.png" alt="" class="Contacts__icon"></picture></a></p>
                </div>
            </div>
        </div>


    </div>
</main>
<div id="map"></div>
<?php get_footer();?>
<?php wp_footer();?>
</body>
</html>