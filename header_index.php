<header class="header_index">
    <div class="menu">
        <div class="logo logo_index">
            <picture><source srcset="<?php echo get_template_directory_uri(); ?>/assets/img/icons/icon__vk_index.webp" type="image/webp"><img class="messenger logo__img" src="<?php echo get_template_directory_uri(); ?>/assets/img/icons/icon__vk_index.png"></picture>
             <picture><source srcset="<?php echo get_template_directory_uri(); ?>/assets/img/icons/icon__telegram_index.webp" type="image/webp"><img class="messenger logo__img" src="<?php echo get_template_directory_uri(); ?>/assets/img/icons/icon__telegram_index.png"></picture>
        </div>
        <div class="menu__body">
            <div class="menu__burger">
                <picture><source srcset="https://img2.freepng.ru/20180429/okq/kisspng-computer-icons-menu-cafe-5ae5cda75d2bc2.8582560415250098313816.jpg" type="image/webp"><img src="https://img2.freepng.ru/20180429/okq/kisspng-computer-icons-menu-cafe-5ae5cda75d2bc2.8582560415250098313816.jpg"></picture>
            </div>

            <?php wp_nav_menu( array(
                'theme_location' => 'header_menu',
                'container' => false,
                'menu_class' =>'menu__list menu__list_index',
            ) );
            ?>
        </div>

    </div>
</header>