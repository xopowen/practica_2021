<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title><?php echo   $wp_query->post->post_title;  ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php wp_head();?>
</head>
<body>
<?php get_header()?>
    <div class="heading">
        <div class="container">
            <div class="heading__body">
                <div class="heading__contant ">
                <h1 class="heading__h1 head_big_text"><?php echo   $wp_query->post->post_title;  ?></h1>
                    <div class="heading__way"> <p><?php echo get_the_title(  get_page_by_path( 'main' ) ); ?> > <?php echo   $wp_query->post->post_title;  ?></p></div>
            </div>
            </div>

        </div>
    </div>

    <main>
        <div class="container">
            <div class="navigation"><p class="navigation card__subtext"><a href="<?php echo get_the_permalink(  get_page_by_path( 'menu' ));?>"> <img src="<?php echo get_template_directory_uri(); ?>/assets/img/vector_smart_object.png"> Назад в <?php echo get_the_title(  get_page_by_path( 'menu' ) ); ?></a></p></div>
            <div class="loyalty">
                <picture><source srcset="<?php echo get_template_directory_uri(); ?>/assets/img/loyalty.webp" type="image/webp"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/loyalty.png" alt=""></picture>
            </div>
            <div class="products">
                <div class="inFrame">
                    <div class="describe inFrame__describe">
                    <?php
//выводим текст в рамке 
$args=array(
    'post_type' => 'text_pole',
    'category_name'=> 'text_in_border_on_loy_prog'
);
//запрос постов 
$text_in_border_on_loy_prog = query_posts($args);
foreach ($text_in_border_on_loy_prog as $post) :

setup_postdata($post);
?>
                        <p class="describe__text">
                            <?php echo get_the_content(); ?>
                        </p>

<?php endforeach; 
?>

                    </div>
                </div>

                <div class="description">
<?php
    //выводим текст 
    $args=array(
        'post_type' => 'text_pole',
        'category_name'=> 'text_on_page_loy_prog'
    );
    //запрос постов 
    $text_on_page_loy_prog = query_posts($args);
    foreach ($text_on_page_loy_prog as $post) :
        setup_postdata($post);
    ?>
    <?php if(get_the_title() !== 'Как получить карту?' ):?>
                    <p class="describe__text "><?php echo get_the_content(); ?>
                    </p>
    <?php endif ?>
<?php endforeach; 
?>

                </div>
            <div class="contant wrap">

            <?php
//сколько карточки преемуществ
$args=array(
    'post_type' => 'advantage',
    'category_name'=> 'advantage_in_loyalty-program'
);
//запрос постов 
$whole_cake = query_posts($args);
global $whole_cake;
foreach ($whole_cake as $post) :

setup_postdata($post);

$thumbnail_attributes = wp_get_attachment_image_src( get_post_thumbnail_id(), 'medium' );

?>
                <div class="advantage advantage_min">
                    <picture><source srcset="<?php echo $thumbnail_attributes[0]; ?>" type="image/webp"><img src="<?php echo $thumbnail_attributes[0]; ?>" alt="" class="advantage__img"></picture>
                    <p class="advantage__text">
                    <?php echo get_the_content(); ?>
                    </p>
                </div>
<?php endforeach; 
?>

            </div>
            <div class="description">
<?php
            //выводим текст c заголовком Как получить карту?
$args=array(
    'post_type' => 'text_pole',
    'category_name'=> 'text_on_page_loy_prog'
);
    //запрос постов 
    $text_on_page_loy_prog = query_posts($args);
    foreach ($text_on_page_loy_prog as $post) :
        setup_postdata($post);
?>
    <?php if(get_the_title() == 'Как получить карту?' ):?>
                    <h3 class="describe__head"><?php the_title()?></h3>
                    <p class="describe__text "><?php echo get_the_content(); ?>
                    </p>
    <?php endif ?>
<?php endforeach; 
?>
                <p class=" describe__text_rules">Правила участия в Программе лояльности</p>
            </div>

            <div class="container">
        <div class="products">
            <div class=" purchase__heading_loyalty">
                <div class="container">
                    <div class="heading__body">
                        <div class="heading__contant ">
                        <h1 class="heading__h1 purchase__h1">Анкета программы лояльности</h1>
                    </div>
                    </div>
                </div>
            </div>
            <div class="products__form">
                <form action="" class="from">
                    <div class="form__discounts">
                        <div class="form__discount">
                            <div class="form__image">
                                <picture><source srcset="<?php echo get_template_directory_uri(); ?>/assets/img/icons/caks.webp" type="image/webp"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/icons/caks.png" alt=""></picture>
                            </div>
                            <input type="radio" class="from__radio" name="caks">Хочу скидку на тортик 
                        </div>
                        <div class="form__discount">
                        <div class="form__image">
                                <picture><source srcset="<?php echo get_template_directory_uri(); ?>/assets/img/icons/coffe.webp" type="image/webp"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/icons/coffe.png" alt=""></picture>
                        </div>
                        <input type="radio" class="from__radio" name="caks">
                                Очень люблю кофе, хочу скидку
                        </div>
                    </div>
                    <input type="hidden" id='name__caks' name="name__caks" value="">
                    <label class="form__label"><input type="text" class="form__pole"> <p class=form__txt>Ваша Фамилия<span class="form__txt_necessarily">*</span></p></label>
                    <label class="form__label"><input type="text" class="form__pole"> <p class=form__txt>Ваше имя<span class="form__txt_necessarily">*</span></p></label>
                    <label class="form__label"><input type="text" class="form__pole"> <p class=form__txt>Ваше отчество<span class="form__txt_necessarily">*</span></p></label>
                    <label class="form__label"><input type="text" class="form__pole"> <p class=form__txt>Мобильный телефон<span class="form__txt_necessarily">*</span></p></label>
                    <label class="form__label"><input type="text" class="form__pole"> <p class=form__txt>E-mail<span class="form__txt_necessarily">*</span></p></label>
                    <p class="form__reminder">* Звездочкой отмечены поля, обязательные для заполнения.</p>
                    <div class="form__discount">
                        <input class="form__checkbox" type="checkbox"><p class="form__reminder">Обработка данных</p>
                    </div>
                        <button type="submit" class="from__button"><p class="from__buttonText">отправка данны</p></button>
                </form>
            </div>
        </div>
        </div>
        </div>
    </main>
</body>
<?php get_footer();?>
<?php wp_footer();?>
</html>