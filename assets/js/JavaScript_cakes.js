
    //обработка собитий клик
    document.addEventListener("mouseover", function (Elemet) {
    let target =Elemet.target;
    if(Elemet.target.classList.contains('card__button')){
        let card__parent = target.closest('.card');
        let card__hovarButton = card__parent.querySelector('.card__hoverButton');
        let  card__main = card__parent.querySelector('.card__main');
        card__hovarButton.classList.add("card__hoverButton_active");
        card__main.classList.add('card__main_active');

        //card__button_add_event_mouseover( target );
        card__button_add_event_click(target);
        
    };
    });

    document.addEventListener("mouseout", function (Elemet) {
        let target =Elemet.target;
        if(target.classList.contains('card__button')){

            let card__parent = target.closest('.card');
            let card__hovarButton = card__parent.querySelector('.card__hoverButton');
            let  card__main = card__parent.querySelector('.card__main');
            card__hovarButton.classList.remove("card__hoverButton_active");
            card__main.classList.remove('card__main_active');
        }

    });


    //функция обработки событий клик
    //выводит заявку на олнайн покупку поверх экрана
    function card__button_add_event_click(element) {
        element.addEventListener('click', function() {
            let card__parent = this.closest('.card');
            let soap__background = document.createElement('div');
            document.body.appendChild(soap__background);
            let purchase = document.createElement('div');
            purchase.className = 'purchase';
            soap__background.className = 'soap__background';
           let link__this_page = document.location.href;

            purchase.innerHTML = '<div class="container">\n' +
                '        <div class="products">\n' +
                '            <div class="heading purchase__heading">\n' +
                '                <div class="container">\n' +
                '                    <div class="heading__body">\n' +
                '                        <div class="heading__contant ">\n' +
                '                        <h1 class="heading__h1 purchase__h1">Онлайн Заказ Торта</h1>\n' +
                '                    </div>\n' +
                '                    </div>\n' +
                '\n' +
                '                </div>\n' +
                '            </div>\n' +
                '\n' +
                '            <div class=" card__top_none purchase__card">\n' +
                '                <div class="card__body card__body_active">\n' +
                '                    <div class="card__header">\n' +
                '                        <div class="card__hoverButton"></div>\n' +
                '                        <img src= "'+card__parent.querySelector(".card__img").attributes.src.nodeValue+'" alt=""  class="card__img purchase__img">\n' +
                '                        \n' +
                '                    </div>\n' +
                '\n' +
                '                <div class="card__main">\n' +
                '                    <div class="card__heading">\n' +
                                        card__parent.querySelector(".card__heading").innerHTML+
                '                    </div>\n' +
                '                    <div class="card__text">\n' +
                                            card__parent.querySelector(".card__text").innerHTML+
                '                    </div>\n' +
                '\n' +
                '                    </div>\n' +
                '                </div>\n' +
                '            </div> \n' +
                '            <div class="card__fooder">\n' +
                                    card__parent.querySelector(".card__fooder").innerHTML+
                '            </div>\n' +
                '            <div class="products__form">\n' +
                '                <form action="" class="from">\n' +
                '<div class="navigation"><p class="card__subtext"><a href="'+link__this_page+'"> <img src="'+document.location.origin+'/wordpress/wp-content/themes/xopowen_practice/assets/img/vector_smart_object.png"> Назад </a></p></div>'+
                '                    <input type="hidden" id=\'name__caks\' name="name__caks" value="'+card__parent.querySelector(".card__heading h3 " ).innerText+'"> \n' +
'                                    <label class="form__label"><input type="text" class="form__pole"> <p class="form__txt">Ваш телефон<span class="form__txt_necessarily">*</span></p></label>\n' +
                '                    <label class="form__label"><textarea class="form__pole form__pole_big"  > </textarea><p class= "form__txt form__txt_big">Комментарии к заказу<span class="form__txt_necessarily"></span></p></label>\n' +
                '                    <div class="form__discount">\n' +
                '                        <input class="form__checkbox" type="checkbox"><p class="form__reminder">Согласен(а) на обработку моих персональных данных</p>\n' +
                '                    </div>\n' +
                '                        <button type="submit" class="from__button"><p class="from__buttonText">отправка данны</p></button>\n' +
                '                </form>\n' +
                '            </div>\n' +
                '        </div>\n' +
                '        </div>';
            document.body.appendChild(purchase);
        });

    }
