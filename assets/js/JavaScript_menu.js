



    //обработка собитий клик
    document.addEventListener("click", function (Elemet) {
        let target =Elemet.target;
        if(Elemet.target.classList.contains('categories__heading') ){
            categories__addInform(Elemet);
        }else if (Elemet.target.classList.contains('card__img'))
        {
            document.body.querySelector('.heading__contant').classList.add('heading__contant_active');
            document.body.querySelector('.products__body').classList.add('products__body_active');
            setTimeout(card__img,1500,Elemet);
            setTimeout(del,3000,'products__body_active','heading__contant_active');
        }

        else if(Elemet.target.parentElement.classList.contains('products__navigation_active')){
            document.body.querySelector('.heading__contant').classList.add('heading__contant_active');
            document.body.querySelector('.products__body').classList.add('products__body_active');
            setTimeout(products__navigation,1500);
            setTimeout(del,3000,'products__body_active','heading__contant_active');
        }

});
    //костыл - удоление класса products__body_active и heading__h1_active
      function del(products__body_active,heading__contant_active){
              document.body.querySelector('.products__body').classList.remove(products__body_active);
              document.body.querySelector('.heading__contant').classList.remove(heading__contant_active);

          };

//удаление всех карточек 
    function del_card(list_cards){
        console.log(list_cards);
        let len_list_cards = list_cards.length
        for(let item = 0; item < len_list_cards ; item++){
            list_cards[item].remove();
        };
    }

    //раскрытие доп информации у при клике на катигории и изменение цвета
    function categories__addInform(Elemet){
        list_cards = document.querySelectorAll('.card');
        let loadmore = document.querySelector("#loadmore a")
        let categories__item_active = document.querySelector('.categories__item_active');
        let target = Elemet.target.parentElement;
         if ( target !== null ){
            if( target.classList.contains('categories__item') && categories__item_active !== null ){
                categories__item_active.classList.remove('categories__item_active');
                target.classList.add("categories__item_active");
                document.body.querySelector('.heading__contant').classList.add('heading__contant_active');
                document.body.querySelector('.products__body').classList.add('products__body_active'); 
                setTimeout(products__navigation,1500);
                setTimeout( del_card,1500,list_cards);
                
                setTimeout(ajax_cat,1500,target.id);
                setTimeout(del,3000,'products__body_active','heading__contant_active');


                


            };

         };
        };

    // анимация карточки товара при клике на картинку
    // меняет заголовок и добовляет элимент в подзаголовок
    function card__img(Elemet){
        let heading__h1 = document.body.querySelector('.heading__h1');
        let products__navigation = document.body.querySelector('.products__navigation');
        let card = document.body.querySelector('.card_active');
        let card__top = document.body.querySelector('.card__top');
        let target = Elemet.target.closest('.card__body');
        let heading__way =  document.body.querySelector('.heading__way');
        let heading__way_add  = document.createElement('p');
        heading__way_add.id = 'heading__way_add';
        heading__h1.innerText = target.querySelector('h3').innerText;
        if ( target !== null ){
            if(target.classList.contains('card__body') && card !== null ){
                card.classList.remove('card_active');
                card.classList.add('card');
                card.children[0].classList.remove('card__body_active');
                target.parentElement.classList.add('card_active');
                target.parentElement.classList.remove('card');
                target.classList.add('card__body_active');
                document.body.querySelector('#heading__way_add').innerText = '>' +  target.querySelector('h3').innerText;
            }else {
                products__navigation.classList.add('products__navigation_active');
                target.parentElement.classList.add('card_active');
                target.parentElement.classList.remove('card');
                target.classList.add('card__body_active');
                card__top.classList.add('card__top_none');
                card__top.innerHTML=target.parentElement.innerHTML;
                document.body.querySelector('.card__titleRecommendation').classList.add('card__titleRecommendation_active');
                heading__way_add.innerHTML = '>' + target.querySelector('h3').innerText;
                heading__way.appendChild(heading__way_add);
            };

        }
    }

    //анимация возврата в меню. делает всё так как было до card__img
    function products__navigation() {
        let heading__way =  document.body.querySelector('.heading__way');
        let card__top = document.body.querySelector('.card__top');
        let card = document.body.querySelector('.card_active');
        let heading__h1 = document.body.querySelector('.heading__h1');
        let products__navigation = document.body.querySelector('.products__navigation');
        let heading__way_add  = document.querySelector('#heading__way_add');
        if(card !==null){
            card.classList.add('card');
            card.classList.remove('card_active');
            card.children[0].classList.remove('card__body_active');
        };
        card__top.classList.remove('card__top_none');
        products__navigation.classList.remove('products__navigation_active');
        if (heading__way_add != null){
        heading__way.removeChild(heading__way_add);
        }
        heading__h1.innerText = "Меню";
        document.body.querySelector('.card__titleRecommendation').classList.remove('card__titleRecommendation_active');
    }
