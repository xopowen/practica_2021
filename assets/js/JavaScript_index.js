

            //функция добавления и удалениия класса
    function add__remove_className(item,class_Name) {
            let slider__element_active = document.querySelector('.'+class_Name);
            if(!item.classList.contains(class_Name)){
                if(slider__element_active == null){
                    item.classList.add(class_Name);
                }else{
                    slider__element_active.classList.remove(class_Name);
                    item.classList.add(class_Name);
                };
            };
        };

    //поиск элементов слайдера
    let slider__elements = {
        slider__list : document.querySelector('.slider__list'),
        slider__arrow_left : document.querySelector('.slider__arrow_left'),
        slider__arrow_right : document.querySelector('.slider__arrow_right'),
        slider__dots : document.querySelector('.slider__dots')
    },
    slider__list__count = slider__elements.slider__list.childElementCount;




    //добавляем кнопки для каждого слайдера
    for(let i = 0; i <slider__list__count; i++ ){
        let dot = document.createElement('div');
        dot.className = 'slider__dot';
        if(i==0){
            dot.classList.add('slider__dot_active');
        }
        dot.id = i;

     //вешаем на них событие клик
        dot.addEventListener('click', function () {
            let dov_active = document.querySelector('.slider__dot_active');
            if(!this.classList.contains('.slider__dot_active')){
                if(dov_active == null){
                    this.classList.add('.slider__dot_active');
                    add__remove_className(slider__elements.slider__list.children[this.id],'slider__element_active');
                }else {
                    dov_active.classList.remove('slider__dot_active');
                    this.classList.add('slider__dot_active');
                    add__remove_className(slider__elements.slider__list.children[this.id],'slider__element_active');
                }
            }

        });


        slider__elements.slider__dots.appendChild(dot);
    }





    //вешаем событие клик на кнопки навигации
    // левая
    slider__elements.slider__arrow_left.addEventListener('click', function () {
    let slider__element_active = document.querySelector('.slider__element_active');
    for ( let i = 0; i < slider__list__count; i++ ){
        if (slider__elements.slider__list.children[i] == slider__element_active){
            let element = i - 1 >= 0 ? i - 1 : slider__list__count-1;
            add__remove_className(slider__elements.slider__list.children[element],'slider__element_active');
            add__remove_className(slider__elements.slider__dots[element],'slider__dot_active')
            break;
        };
        };
    });
    //правая
    slider__elements.slider__arrow_right.addEventListener('click', function () {
    let slider__element_active = document.querySelector('.slider__element_active');
    for ( let i = 0; i < slider__list__count; i++ ){
        if ( slider__elements.slider__list.children[i] == slider__element_active ){
            let element = i + 1 < slider__list__count ? i+1: 0;
            add__remove_className(slider__elements.slider__list.children[element],'slider__element_active');
            add__remove_className(slider__elements.slider__dots.children[element],'slider__dot_active')
            break;
        };
        };
    });

