window.onload  = function () {
//функция определения поддержки WebP
    function testWebP(callback) {

        var webP = new Image();
        webP.onload = webP.onerror = function () {
            callback(webP.height == 2);
        };
        webP.src = "data:image/webp;base64,UklGRjoAAABXRUJQVlA4IC4AAACyAgCdASoCAAIALmk0mk0iIiIiIgBoSygABc6WWgAA/veff/0PP8bA//LwYAAA";
    }
    testWebP(function (support) {

    if (support == true) {
        document.querySelector('body').classList.add('webp');
    }else{
        document.querySelector('body').classList.add('no-webp');
    }
    });


    //анимкция menu__burger
    if (document.body.querySelector('.menu__burger')) {
        let menu__burger = document.body.querySelector('.menu__burger');
        let menu__list = document.body.querySelector('.menu__list');
        menu__burger.addEventListener('click', function () {
            if (menu__list.classList.contains("open-menu")) {
                menu__list.classList.remove("open-menu");
            } else {
                menu__list.classList.add("open-menu");
            };
        });
    }
    //анимкция categories__burger
    if (document.body.querySelector('.categories__burger')) {
        let categories__burger = document.body.querySelector('.categories__burger');
        let categories = document.body.querySelector('.categories');
        //анимация categories__burger
        const burgerCategories__click = categories__burger.addEventListener('click', function () {
            if (categories.classList.contains("open-menu")) {
                categories.classList.remove("open-menu");
            } else {
                categories.classList.add("open-menu");
            }
        });
    }
}

