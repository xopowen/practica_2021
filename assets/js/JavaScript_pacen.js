jQuery(function($){
	var bottomOffset = 2000,  // отступ от нижней границы сайта, до которого должен доскроллить пользователь, чтобы подгрузились новые посты
	button = $( '#loadmore a' ),
	paged = button.data( 'paged' ),
	maxPages = button.data( 'max_pages' );
	post_type = button.data('post_type');
	categories = button.data('categories');
	$(window).scroll(function(){
		console.log(window.wp_data.ajax_url)
 
		if( $(document).scrollTop() > ($(document).height() - bottomOffset)  && !$('body').hasClass('loading')){
			$.ajax({
				type : 'POST',
				url : window.wp_data.ajax_url, // получаем из wp_localize_script()
				
				data : {
					paged : paged, // номер текущей страниц
					action : post_type, // экшен для wp_ajax_ и wp_ajax_nopriv_
					categorie : categories
				},
				
				beforeSend : function( xhr ) {
					$('body').addClass('loading');
				},
				success : function( data ){
					 if( data ) {
						paged++;
						console.log(paged)
						$('.products__body').append( data );
						$('body').removeClass('loading');
					}
	 
				}
	 
			});
		}
	});
});

function ajax_cat(categorie){ jQuery(function($){
	console.log(categorie)
			$.ajax({
				type : 'POST',
				url : window.wp_data.ajax_url, // получаем из wp_localize_script()
				
				data : {
					action : 'categorie', // экшен для wp_ajax_ и wp_ajax_nopriv_
					categories : categorie
				},

				success : function( data ){
					 if( data ) {
						$('.products__body').append( data );
					}
	 
				}
	 
			});
});}